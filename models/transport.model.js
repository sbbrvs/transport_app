const mongoose = require('mongoose')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const validate = require('mongoose-validator')
const CONFIG = require('../config/config')
const {isNull} = require('../services/util.service')
const {TE, to} = require('../services/util.service')
const Schema = mongoose.Schema

const TransportSchema = new mongoose.Schema({
   
    

    vehiclenumber: {
        type: String,
    },
    checkInTime:  {
        type: String,
    },
    checkOutTime:  {
        type: String,
    },
    purpose:  {
        type: String,
    },
    
    driver:{
        ref: 'User',
        type: Schema.Types.ObjectId
    },

    verified: {
        type: Boolean,
        default: true,
    }
   ,
    enabled:{
        default:true,
        type: Boolean,

    }
}, {timestamps: true})

TransportSchema.pre('save', async function (next) {return next()})




TransportSchema.methods.toWeb = function () {
    let json = this.toJSON()
    json.id = this._id//this is for the front end
    delete json.password
    
    return json
}

const Transport = module.exports = mongoose.model('Transport', TransportSchema)
