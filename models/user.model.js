const mongoose = require('mongoose')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const validate = require('mongoose-validator')
const CONFIG = require('../config/config')
const {isNull} = require('../services/util.service')
const {TE, to} = require('../services/util.service')
const Schema = mongoose.Schema

const UserSchema = new mongoose.Schema({
    phone: {
        type: String, 
        required: true
    },
    name: {
        type: String,
    },
    email:{
        type: String,
        required:'true'
    },

    password: {
        type: String,
        required:'true'
    },
    
    
    user:{
        ref: 'User',
        type: Schema.Types.ObjectId
    },

    verified: {
        type: Boolean,
        default: true,
    }
   ,
    enabled:{
        default:true,
        type: Boolean,

    }
}, {timestamps: true})

UserSchema.pre('save', async function (next) {return next()})



UserSchema.methods.getJWT = function () {
    let expiration_time = parseInt(CONFIG.jwt_expiration)
    return 'Bearer ' + jwt.sign({user_id: this._id}, CONFIG.jwt_encryption,
        {expiresIn: expiration_time})
}

UserSchema.methods.toWeb = function () {
    let json = this.toJSON()
    json.id = this._id//this is for the front end
    delete json.password
    
    return json
}

const User = module.exports = mongoose.model('User', UserSchema)
