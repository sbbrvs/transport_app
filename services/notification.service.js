const moment = require('moment')
var Mailgun = require('mailgun-js')
const CONFIG = require('../config/config');
var mailgun = new Mailgun(
    {apiKey: 'CONFIG.mg_key',
        domain: 'CONFIG.mg_domain'})


exports.sendEmail = function (destination, options) {
    
    var data = {
        //Specify email data
        from: 'Saas planet<hello@proxiemo.com>',
        //The email to contact
        to: destination,
        //Subject and text data
        subject: options.subject,
        html: options.body
    }
    
    if (options.cc) {
        data.cc = options.cc
    }
    
    return new Promise((resolve, reject) => {
    
        if (CONFIG.send_email === 'false') {
            var message = "Not sending Email, Email Disabled"
            reject(new Error(message))
            return
        }
        mailgun.messages().send(data, function (err, response) {

            //If there is an error, render the error page
            if (err) {
                reject(err)
                return
            }
            resolve({
                destination: destination,
                response: response
            })


        })
    })


}
