const { User } = require('../models')
// const authService = require('../services/auth.service');
const mongoose = require('mongoose')
const { isNull, isEmpty } = require('../services/util.service')
const { to, ReE, ReS } = require('../services/util.service')
const authService = require('../services/auth.service')
const notificationService = require('../services/notification.service')
const validator = require('validator')
const { isEmail ,isMobilePhone } = validator
const ObjectId = require('mongoose').Types.ObjectId
const moment = require('moment')
const HttpStatus = require('http-status')
const CONFIG = require('../config/config')

const create = async function (req, res) {

    const body = req.body
    let err, user


    if(isNull(body.name) || body.name.length < 3) {
        return ReE(res, 'please enter a username with minimum 3 characters', 400)
    } else if (isNull(body.phone)) {
        return ReE(res, 'please enter your phone', 400)
    } else if (isNull(body.password) || body.password.length < 8) {
        return ReE(res, 'please enter a password with minimum 8 characters',
            400)
    }
    else if (isNull(body.email) ||!isEmail(body.email)) {
        return ReE(res, 'please enter a valid email ',
            400)
    }
    
    [err, user] = await to(authService.createUser(body))
    if (err) return ReE(res, err, 422)

    return ReS(res, {
        message: 'User Created',
        user: {
            name: user.name,
            email: user.email,
            type:user.role
        },
    }, 201)



}
module.exports.create = create




const login = async function (req, res) {
    let err, user
    const phone = req.body.phone
    const password = req.body.password


    if (isNull(phone)) {
        return ReE(res, { message: 'Please enter phone' }, 400)
    }

    if (isNull(password)) {
        return ReE(res, { message: 'Please enter your password to login' }, 400)
    }

    [err, user] = await to(User.findOne({ phone: phone, enabled: true ,password:password}))

    if (err) return ReE(res, err, 400)

    if (!user) return ReE(res,
        { message: 'invalid phone or password' },
        400)



    ReS(res, {
        message: 'User logged in ',
        user: user,

        token: user.getJWT(),
    }, 200)

}
module.exports.login = login



