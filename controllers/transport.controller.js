
const { Transport } = require('../models')

const validator = require('validator')

const { to, ReE, ReS } = require('../services/util.service')

const checkIn = async (req, res) => {

    let {vehiclenumber,purpose} = req.body;

    if(!vehiclenumber) return ReE(res, 'vehiclenumber field required', 422)
    if(!purpose) return ReE(res, 'purpose field required', 422)

   

    let checkin,err;

    [err,checkin] = await to(Transport.create({checkInTime:new Date(),vehiclenumber:vehiclenumber,purpose:purpose,
    driver:req.user._id,
    }));

   if(err) return ReE(res, err.message, 500)

   return ReS(res, {message:"Checkin data added",checkin}, 200)

}


const checkOut = async (req, res) => {

    let {vehiclenumber,purpose} = req.body;

    if(!vehiclenumber) return ReE(res, 'vehiclenumber field required', 422)
    
    if(!purpose) return ReE(res, 'purpose field required', 422)

   

    let checkOut,err;

    [err,checkOut] = await to(Transport.findOne({vehiclenumber:vehiclenumber,
    driver:req.user._id,checkOutTime:{ $exists: false}
    }));

   if(err) return ReE(res, err.message, 500)



   if(!checkOut) return ReE(res, {message:"Checkin Data not found..!"}, 404)

   checkOut.checkOutTime = new Date() ;
   
   [err,checkOut] = await to(checkOut.save());

   if(err) return ReE(res, err.message, 500)

   return ReS(res, {message:"CheckOut data added",checkOut}, 200);

}

const checkDriver = async(req,res) =>{

    let checkOut,err;

    [err,checkOut] = await to(Transport.findOne({driver:req.user._id,checkOutTime:{ $exists: false}
    }));

   if(err) return ReE(res, err.message, 500)

   if(!checkOut) return ReS(res, {message:"user not checkin",checkoutStatus:true}, 200);
   
   return ReS(res, {message:"user not checkout",checkoutStatus:false}, 200);

   

}

module.exports = {
    checkDriver:checkDriver,
    checkIn:checkIn,
    checkOut:checkOut
}