const express = require('express')
const router = express.Router()

const UserController = require('../controllers/user.controller')

const TransportController = require('../controllers/transport.controller')


const passport = require('passport')

const needsAuth = passport.authenticate('jwt', { session: false })
require('./../middleware/passport')(passport)

router.get('/', (req, res) => {
    return res.json({ message: 'Nothing here in this route' })
})

router.post('/register', UserController.create)

router.post('/login', UserController.login)

router.post('/checkin',needsAuth, TransportController.checkIn)

router.post('/checkout',needsAuth, TransportController.checkOut)

router.get('/driverstatus',needsAuth, TransportController.checkDriver)

module.exports = router
