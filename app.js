
const express = require('express')

const logger = require('morgan')

const mongoose = require('mongoose')

const bodyParser = require('body-parser')

const cors = require('cors')

var CronJob = require('cron').CronJob;

const pe = require('parse-error')



const config = require('./config/config')

mongoose.set('useCreateIndex', true)

const models = require('./models')

const app = express()

const PORT = process.env.PORT || 3000

app.use(logger('combined'))

app.use(cors())

app.use(bodyParser.json())

app.get('/', (req, res) => {
    return res.json({
        message: 'Hello! Welcome to Transport Managment',
    })
})


const v1 = require('./routes/v1')
app.use('/v1', v1)



app.listen(PORT, () => {
    console.log('Server started on port', PORT)
})

to = function (promise) {
    return promise
        .then(data => {
            return [null, data]
        }).catch(err =>
            [err],
        )
}
