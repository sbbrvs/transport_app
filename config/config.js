require('dotenv').config();//instatiate environment variables

let CONFIG = {} //Make this global to use all over the application

CONFIG.app          = process.env.APP   || 'development';
CONFIG.port         = process.env.PORT  || '3200';

// CONFIG.db_uri  = process.env.MONGODB_URI   || 'mongodb://heroku_bzjjndcp:fo7gugaccaho0ilu2h825g3vp2@ds143683.mlab.com:43683/heroku_bzjjndcp';

CONFIG.db_uri  = process.env.MONGODB_URI   || 'mongodb://127.0.0.1:27017/test';

// CONFIG.db_uri  = process.env.MONGODB_URI   || 'mongodb://127.0.0.1:27017/medicines';

CONFIG.jwt_encryption  = process.env.JWT_ENCRYPTION || 'jwt_please_change';
CONFIG.jwt_expiration  = process.env.JWT_EXPIRATION || '28800';


module.exports = CONFIG;
